package amazons.ai;

import amazons.client.Move;
import amazons.state.Segment;
import amazons.state.SegmentSet;
import amazons.state.SegmentState;

import java.util.LinkedList;
import java.util.List;

/**
 */

public class FillingMoveSet {
    private final Object threadControlMutex = new Object();

    private final List<FillingMoveStore> fillingMoveStores;

    private List<Move> currentMoveList;

    public FillingMoveSet() {
        fillingMoveStores = new LinkedList<>();
        currentMoveList = new LinkedList<>();
    }

    public void startCalculating() {
        synchronized (threadControlMutex) {
            System.out.println("Starting " + fillingMoveStores.size() + " fillers.");
            for (FillingMoveStore store : fillingMoveStores) {
                store.startCalculating();
            }
        }
    }

    public void pauseCalculating() {
        synchronized (threadControlMutex) {
            System.out.println("Pausing " + fillingMoveStores.size() + " fillers.");
            for (FillingMoveStore store : fillingMoveStores) {
                store.pauseCalculating();
            }
        }
    }

    public Move getMove() {
        if (fillingMoveStores.isEmpty()) {
            throw new RuntimeException("No more stores to get moves from!");
        } else if (currentMoveList.isEmpty()) {
            FillingMoveStore selectedStore = null;
            // try to see if any are complete.
            for (FillingMoveStore store : fillingMoveStores) {
                if (store.isComplete()) {
                    selectedStore = store;
                    currentMoveList = selectedStore.getBestMoves();
                    break;
                }
            }
            if (selectedStore == null) {
                selectedStore = fillingMoveStores.get(0);
                currentMoveList = selectedStore.getBestMovesSoFar();
            }
            if (fillingMoveStores.size() > 1) fillingMoveStores.remove(selectedStore);
            if (currentMoveList.isEmpty()) {
                throw new RuntimeException("This store has an empty list of moves...?");
            } else {
                return getMove();
            }
        } else {
            Move nextMove = currentMoveList.remove(0);
            return nextMove;
        }
    }

    public void registerOurMove(Move move, SegmentSet currentSet) {
        Segment relevantSegment = currentSet.getContainingSegment(move.getFromIndex());
        List<Segment> newSegments = relevantSegment.forkMove(move, false);
        for (Segment segment : newSegments) {
            System.out.println(segment.getSegmentState() + " " + segment.enclosedCount());
            if (segment.getSegmentState() == SegmentState.WHITE_OWNED) {
                fillingMoveStores.add(new FillingMoveStore(segment));
            }
        }
    }

    public void registerOpponentMove(Move move, SegmentSet currentSet) {
        Segment relevantSegment = currentSet.getContainingSegment(move.getFromIndex());
        List<Segment> newSegments = relevantSegment.forkMove(move, true);
        // If opponent forces new segments check if we own any of them.
        for (Segment segment : newSegments) {
            if (segment.getSegmentState() == SegmentState.WHITE_OWNED) {
                fillingMoveStores.add(new FillingMoveStore(segment));
            }
        }
    }
}
