package amazons.ai;

import amazons.client.Move;
import amazons.state.Segment;
import amazons.state.SegmentSet;

import java.util.List;

/**
 *
 */
public class Heuristic {
    public final int friendlyQueenMoveCoeff, friendlyKingMoveCoeff, enemyQueenMoveCoeff, enemyKingMoveCoeff;

    public Heuristic(int friendlyQueenMoveCoeff, int friendlyKingMoveCoeff, int enemyQueenMoveCoeff, int enemyKingMoveCoeff) {
        this.friendlyQueenMoveCoeff = friendlyQueenMoveCoeff;
        this.friendlyKingMoveCoeff = friendlyKingMoveCoeff;
        this.enemyQueenMoveCoeff = enemyQueenMoveCoeff;
        this.enemyKingMoveCoeff = enemyKingMoveCoeff;
    }

    public Heuristic() {
        friendlyQueenMoveCoeff = 1;
        friendlyKingMoveCoeff = 1;
        enemyQueenMoveCoeff = 1;
        enemyKingMoveCoeff = 1;
    }

    public int score(SegmentSet ps) {
        int whiteOwnedSegmentsScore = rateAllOwnedSegments(ps.getWhiteOwnedSegments());
        int blackOwnedSegmentsScore = rateAllOwnedSegments(ps.getBlackOwnedSegments());
        int contestedSegmentsScore = rateAllContestedSegments(ps.getContestedSegments());
        return (whiteOwnedSegmentsScore + blackOwnedSegmentsScore + contestedSegmentsScore);
    }

    public int rateAllOwnedSegments(List<Segment> pl) {
        int totalReachableSpaces = 0;
        for (Segment p : pl)
            totalReachableSpaces += rateOwnedSegment(p);
        return totalReachableSpaces;
    }

    public int rateAllContestedSegments(List<Segment> pl) {
        int contestedSegmentScores = 0;
        for (Segment p : pl)
            contestedSegmentScores += rateContestedSegments(p);
        return contestedSegmentScores;
    }

    public int rateOwnedSegment(Segment p) {
        int reachableSpaces = p.enclosedCount();
        reachableSpaces -= p.getBlackQueens().size();
        reachableSpaces -= p.getWhiteQueens().size();
        return reachableSpaces;
    }

    public int rateContestedSegments(Segment p) {
        int enemyScore = rateQueens(p, false);
        int friendlyScore = rateQueens(p, true);
        return (friendlyScore - enemyScore);
    }

    public int rateQueensSet(List<Segment> pl, boolean white) {
        int queenScore = 0;
        for (Segment p : pl)
            queenScore += rateQueens(p, white);
        return queenScore;
    }

    public int rateQueens(Segment p, boolean white) {
        List<Integer> queens = null;
        if (white)
            queens = p.getWhiteQueens();
        else
            queens = p.getBlackQueens();
        int queenScore = 0;
        for (Integer q : queens)
            queenScore += rateNode(p, q, white);
        return queenScore;
    }

    public int rateMove(Segment pre, Move m) {
        List<Segment> postMoveSegments = pre.forkMove(m, true);
        Segment post = pre;
        for (Segment p : postMoveSegments)
            if (p.containsNode(m.getToIndex()))
                post = p;
        return (rateTo(post, m.getToIndex()) - rateFrom(pre, m.getFromIndex()) + rateShot(pre, postMoveSegments));
    }

    public int rateFrom(Segment p, int i) {
        return rateNode(p, i, true);
    }

    public int rateTo(Segment p, int i) {
        return rateNode(p, i, true);
    }

    public int rateShot(Segment pre, List<Segment> postShotSegmens) {
        int preShot = ratePreShot(pre);
        int postShot = ratePostShot(postShotSegmens);
        return (preShot - postShot);
    }

    public int ratePreShot(Segment p) {
        int enemyQueenValues = rateQueens(p, false);
        int friendlyQueenValues = rateQueens(p, true);
        return (enemyQueenValues - friendlyQueenValues);
    }

    public int ratePostShot(List<Segment> segments) {
        int enemyQueenValues = rateQueensSet(segments, false);
        int friendlyQueenValues = rateQueensSet(segments, true);
        return (enemyQueenValues - friendlyQueenValues);
    }

    public int rateNode(Segment p, int i, boolean white) {
        int kingMoves = p.getNeighboringIndicies(i).size();
        int queenMoves = p.getReachableIndicies(i).size();
        if (white) {
            kingMoves *= friendlyKingMoveCoeff;
            queenMoves *= friendlyQueenMoveCoeff;
        } else {
            kingMoves *= enemyKingMoveCoeff;
            queenMoves *= enemyQueenMoveCoeff;
        }
        return (kingMoves + queenMoves);
    }
}
