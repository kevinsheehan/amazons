package amazons.ai;

import java.util.*;

/**
 */

public class MoveStore {
    private SortedSet<AggregateMove> currentValues;

    private final List<MoveStore> children;

    private final AggregateMove rootMove;
    private final boolean isMinimizing;

    public MoveStore(AggregateMove rootMove, boolean isMinimizing) {
        currentValues =
                Collections.synchronizedSortedSet(new TreeSet<AggregateMove>());
        children = new LinkedList<>();
        this.rootMove = rootMove;
        this.isMinimizing = isMinimizing;
    }

    public AggregateMove getRootMove() {
        return rootMove;
    }

    public void addChild(MoveStore child) {
        children.add(child);
    }

    public List<MoveStore> getChildren() {
        return Collections.unmodifiableList(children);
    }

    public void addMove(AggregateMove move) {
        currentValues.add(move);
    }

    public AggregateMove getBestSoFar() {
        if (currentValues.size() == 0) {
            return rootMove;
        } else {
            Set<AggregateMove> newValues = new HashSet<>();
            for (MoveStore store : children) {
                newValues.add(store.getBestSoFar());
            }

            if (newValues.size() > 0) {
                int completedTotal = 0;
                AggregateMove bestMove = null;
                int bestScore;
                if (isMinimizing) {
                    bestScore = Integer.MAX_VALUE;
                } else {
                    bestScore = Integer.MIN_VALUE;
                }
                Map<AggregateMove, Integer> newScoreMap = new HashMap<>();
                for (AggregateMove m : newValues) {
                    newScoreMap.put(m, m.getValue());
                }
                for (AggregateMove m : currentValues) {
                    if (newValues.contains(m)) {
                        completedTotal += newScoreMap.get(m) - m.getValue();
                    }
                }
                int meanDelta = completedTotal / newValues.size();
                for (AggregateMove m : currentValues) {
                    int tempScore;
                    if (newScoreMap.containsKey(m)) {
                        tempScore = newScoreMap.get(m);
                    } else {
                        tempScore = m.getValue() + meanDelta;
                    }
                    // That ^ is the XOR operator.
                    // isMinimizing    (tempScore > bestScore)     result
                    //         false                      false      false
                    //         false                       true       true
                    //          true                      false       true
                    //          true                       true      false
                    if (isMinimizing ^ (tempScore > bestScore)) {
                        bestScore = tempScore;
                        bestMove = m;
                    }
                }
                return bestMove;
            } else {
                if (isMinimizing) {
                    return currentValues.first();
                } else {
                    return currentValues.last();
                }
            }
        }
    }
}
