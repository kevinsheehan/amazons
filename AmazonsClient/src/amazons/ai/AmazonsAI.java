package amazons.ai;

import amazons.client.Move;
import amazons.state.NodeSet;
import amazons.state.Segment;
import amazons.state.SegmentSet;

import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 *
 */
public class AmazonsAI {
    protected Heuristic scorer;
    protected Timer timer;
    protected final FillingMoveSet fillingSet;
    protected int initialSearchWidth;
    protected int searchWidth;

    private final Object stateMutex = new Object();
    private final Object timerMutex = new Object();
    private final Object notifyMutex = new Object();

    private boolean timeExpired;

    protected SegmentSet segmentSet;
    protected int moveCount = -1;

    public AmazonsAI(boolean isPlayerBlack, Heuristic scorer, Timer timer,
                     int searchWidth) {
        segmentSet = new SegmentSet(new NodeSet(isPlayerBlack).getRootPartition());
        this.scorer = scorer;
        this.searchWidth = searchWidth;
        initialSearchWidth = searchWidth;
        this.timer = timer;
        fillingSet = new FillingMoveSet();
    }

    public AmazonsAI(boolean isPlayerBlack, Heuristic scorer, Timer timer,
                     int searchWidth, int initialSearchWidth) {
        segmentSet = new SegmentSet(new NodeSet(isPlayerBlack).getRootPartition());
        this.scorer = scorer;
        this.searchWidth = searchWidth;
        this.initialSearchWidth = initialSearchWidth;
        this.timer = timer;
        fillingSet = new FillingMoveSet();
    }
    
    protected void startIdling() {
        fillingSet.startCalculating();
    }
    
    protected void stopIdling() {
        fillingSet.pauseCalculating();
    }

    public void opponentMove(Move move) {
        moveCount++;
        fillingSet.registerOpponentMove(move, segmentSet);
        segmentSet = segmentSet.forkSegmentSet(move, true).isolate();
        segmentSet.getBoardPrintout();
    }

    public Move getPlayerMoveAbstract(int timeRemaining) {
        moveCount++;
        // Stop idling.
        stopIdling();
        System.out.println(segmentSet.getSummaryInfo());
        System.out.println(segmentSet.getBoardPrintout());
        Move move = getPlayerMove(timeRemaining);
        segmentSet = segmentSet.forkSegmentSet(move, false).isolate();
        // Start idling.
        startIdling();
        return move;
    }

    protected Move getPlayerMove(int timeRemaining) {
        synchronized (stateMutex) {
            // Set the timer.
            final int timeGiven = timer.getMillisecondsAvailable(timeRemaining, moveCount);
            Thread timerThread = new Thread(new Runnable() {
                public void run() {
                    synchronized (timerMutex) {
                        timeExpired = false;
                        try {
                            timerMutex.wait(timeGiven);
                        } catch (InterruptedException e) {

                        }
                        synchronized (notifyMutex) {
                            timeExpired = true;
                            notifyMutex.notifyAll();
                        }
                    }
                }
            });
            timerThread.setPriority(Thread.MAX_PRIORITY);
            timerThread.start();
            // Start with the contested blocks.
            if (segmentSet.areAnyContestedSegments()) {
                // There are contested parts - get a move.
                return getPlayerMoveCore(timeRemaining);
            } else {
                // There are no contested parts - we are filling.
                return getPlayerMoveFilling(timeRemaining);
            }
        }
    }
    
    protected Move getPlayerMoveCore(int timeRemaining) {
        List<Segment> contestedSegments = segmentSet.getContestedSegments();
        // Get a list of all our moves.
        SortedSet<AggregateMove> scoredMoves = Collections.synchronizedSortedSet(new TreeSet<AggregateMove>());
        // Launch a new thread for each partition/queen.
        // There is a maximum of 4 moving queens.
        List<Thread> evalThreadList = new LinkedList<>();
        for (Segment segment : contestedSegments) {
            for (Integer queenIndex : segment.getWhiteQueens()) {
                Thread evalThread = new EvalQueenThread(segment, queenIndex, scoredMoves, segmentSet, false);
                evalThreadList.add(evalThread);
                evalThread.start();
            }
        }
        // Join with all threads.
        try {
            for (Thread t : evalThreadList) {
                t.join();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Thread interrupt while evaluating initial moves!");
        }
        
        // We need to launch a thread for each of the top initialSearchWidth moves.
        // Create the global store.
        final InitialMoveStore initialStore = new InitialMoveStore();
        // Create a sorted list of all moves.
        List<AggregateMove> allMoves = new ArrayList<>(scoredMoves);
        // If there are no moves, delegate to the filler.
        if (allMoves.isEmpty()) {
            return getPlayerMoveFilling(timeRemaining);
        }
        
        // Sort the moves into 4 lists.
        List<AggregateMove> threadMoves1 = new LinkedList<>();
        List<AggregateMove> threadMoves2 = new LinkedList<>();
        List<AggregateMove> threadMoves3 = new LinkedList<>();
        List<AggregateMove> threadMoves4 = new LinkedList<>();
        int assignment = 0;
        for (int i = 1; (i <= initialSearchWidth) && (i < allMoves.size()); i++) {
            AggregateMove placementMove = allMoves.get(allMoves.size() - i);
            initialStore.addMove(allMoves.get(allMoves.size() - i));
            switch (assignment) {
                case 0:
                    threadMoves1.add(placementMove);
                case 1:
                    threadMoves2.add(placementMove);
                case 2:
                    threadMoves3.add(placementMove);
                case 3:
                    threadMoves4.add(placementMove);
            }
            assignment = (assignment + 1) % 4;
        }
        
        final CyclicBarrier finishGameTreeBarrier = new CyclicBarrier(5);
        
        new Thread(new Runnable() {
            public void run() {
                try {
                    finishGameTreeBarrier.await();
                } catch (InterruptedException e) {
                    
                } catch (BrokenBarrierException e) {
                    
                }
                synchronized(timerMutex) {
                    timerMutex.notifyAll();
                }
            }
        }).start();
        
        // Create threads.
        Thread searchThread1 = new SearchThread(threadMoves1, initialStore, finishGameTreeBarrier);
        Thread searchThread2 = new SearchThread(threadMoves2, initialStore, finishGameTreeBarrier);
        Thread searchThread3 = new SearchThread(threadMoves3, initialStore, finishGameTreeBarrier);
        Thread searchThread4 = new SearchThread(threadMoves4, initialStore, finishGameTreeBarrier);
        
        // Start threads.
        searchThread1.start();
        searchThread2.start();
        searchThread3.start();
        searchThread4.start();
        
        // Wait for the time to run out.
        synchronized (notifyMutex) {
            if (!timeExpired) {
                try {
                    notifyMutex.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        finishGameTreeBarrier.reset();
        
        // Stop the threads.
        searchThread1.interrupt();
        searchThread2.interrupt();
        searchThread3.interrupt();
        searchThread4.interrupt();
        
        // Let them clean up.
        try {
            searchThread1.join();
            searchThread2.join();
            searchThread3.join();
            searchThread4.join();
        } catch (InterruptedException e) {
            // should not occur
        }
        
        // Return the best move we have.
        AggregateMove bestMove = initialStore.getBestSoFar();
        System.out.println("Best move: " +
                bestMove.getMove().getSendingPrintout() + " [" +
                bestMove.getValue() + "]");
        
        Move m = bestMove.getMove();
        fillingSet.registerOurMove(m, segmentSet);
        return m;
    }
    
    protected Move getPlayerMoveFilling(int timeRemaining) {
        return fillingSet.getMove();
    }

    
    protected class EvalQueenThread extends Thread {
        private final Segment segment;
        private final int queenIndex;
        private final SortedSet<AggregateMove> moveSet;
        private final SegmentSet segmentSet;
        private final boolean isMovingPlayerBlack;
        public EvalQueenThread(Segment segment, int queenIndex,
                SortedSet<AggregateMove> moveSet, SegmentSet segmentSet,
                boolean isMovingPlayerBlack) {
            this.segment = segment;
            this.queenIndex = queenIndex;
            this.moveSet = moveSet;
            this.segmentSet = segmentSet;
            this.isMovingPlayerBlack = isMovingPlayerBlack;
        }
        
        public void run() {
            for (Move possibleMove : segment.getPossibleMoves(queenIndex)) {
                SegmentSet newPartSet = segmentSet.forkSegmentSet(
                        possibleMove, isMovingPlayerBlack);
                moveSet.add(new AggregateMove(possibleMove, segment, newPartSet,
                        scorer.score(newPartSet)));
            }
        }
    }

    protected class SearchThread extends Thread {
        private final InitialMoveStore globalStore;
        private final List<AggregateMove> threadMoves;
        private final List<MoveStore> threadStore;
        private final CyclicBarrier barrier;

        int levelsTraversed = 0;

        public SearchThread(List<AggregateMove> threadMoves,
                InitialMoveStore globalStore, CyclicBarrier barrier) {
            this.globalStore = globalStore;
            this.threadMoves = threadMoves;
            threadStore = new LinkedList<>();
            this.barrier = barrier;
        }

        @Override
        public void run() {
            boolean isBlackMoving = true;
            // For each segment set, create a new move store for it.
            for (AggregateMove m : threadMoves) {
                // The first level of moves stores are MINIMIZING.
                MoveStore newStore = new MoveStore(m, isBlackMoving);
                threadStore.add(newStore);
                globalStore.addChild(newStore);
            }
            // For each segment set, launch a new call to evaluate it.
            List<MoveStore> currentStores = threadStore;
            List<MoveStore> nextStores = new LinkedList<>();

            int confirmedMoves = 0;
            int pendingMoves = 0;
            while (!Thread.currentThread().isInterrupted()) {
                if (currentStores.isEmpty()) {
                    try {
                        barrier.await();
                    } catch (InterruptedException e) {

                    } catch (BrokenBarrierException e) {

                    }
                    break;
                }
                isBlackMoving = !isBlackMoving;
                for (MoveStore currentStore : currentStores) {
                    if (Thread.currentThread().isInterrupted()) {
                        break;
                    } else {
                        SortedSet<AggregateMove> nextMoves =
                                new TreeSet<>();
                        for (Segment segment : currentStore.getRootMove().getSegmentSet().getContestedSegments()) {
                            if (Thread.currentThread().isInterrupted()) {
                                break;
                            } else {
                                for (int queenIndex : (isBlackMoving ? segment.getBlackQueens() : segment.getWhiteQueens())) {
                                    if (Thread.currentThread().isInterrupted()) {
                                        break;
                                    } else {
                                        for (Move move : segment.getPossibleMoves(queenIndex)) {
                                            if (Thread.currentThread().isInterrupted()) {
                                                pendingMoves = nextMoves.size();
                                                break;
                                            } else {
                                                SegmentSet newSegmentSet =
                                                        currentStore.getRootMove().getSegmentSet().forkSegmentSet(segment, move, isBlackMoving);
                                                nextMoves.add(new AggregateMove(move, segment, newSegmentSet, scorer.score(newSegmentSet)));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // only take the best searchWidth of them.
                        List<AggregateMove> moveList = new ArrayList<>(nextMoves);
                        if (isBlackMoving) {
                            // we're trying to minimize - take the FIRST moves.
                            for (int i = 0; (i < searchWidth) && (i < moveList.size()); i++) {
                                MoveStore nextStore =
                                        new MoveStore(moveList.get(i), isBlackMoving);
                                nextStores.add(nextStore);
                                currentStore.addChild(nextStore);
                                currentStore.addMove(moveList.get(i));
                            }
                        } else {
                            // we're trying to minimize - take the LAST moves.
                            for (int i = 1; (i <= searchWidth) && (i <= moveList.size()); i++) {
                                MoveStore nextStore = new MoveStore(moveList.get(moveList.size() - i), isBlackMoving);
                                nextStores.add(nextStore);
                                currentStore.addChild(nextStore);
                                currentStore.addMove(moveList.get(moveList.size() - i));
                            }
                        }
                    }
                }
                if (Thread.currentThread().isInterrupted()) {
                    confirmedMoves = nextStores.size();
                    break;
                } else {
                    currentStores = nextStores;
                    nextStores = new LinkedList<>();
                    levelsTraversed++;
                    globalStore.waitForOtherThreads();
                }
            }
            //System.out.println("Thread Report: " + levelsTraversed + "." + confirmedMoves + "." + pendingMoves);
        }
    }
}
