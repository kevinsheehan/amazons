package amazons.ai;

import amazons.client.Move;
import amazons.state.Segment;
import amazons.state.SegmentSet;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Filling move store creates a Stack and searches for the best way to fill.
 * Then it just pops the move off of the stack when we are filling.
 */
public class FillingMoveStore {
    private final SegmentSet baseSet;
    private boolean isComplete;
    private Thread fillingMoveRunner;
    private final int optimalDepth;

    private List<Move> bestMoves;
    private Stack<Move> currentMoves;
    private Stack<SegmentSet> currentStates;
    private Stack<Stack<Move>> potentialMoves;

    public FillingMoveStore(Segment baseSegment) {
        baseSet = new SegmentSet(baseSegment);
        isComplete = false;
        optimalDepth = baseSegment.getFreeStates();
        bestMoves = new LinkedList<>();
        currentMoves = new Stack<>();
        currentStates = new Stack<>();
        potentialMoves = new Stack<>();
        currentStates.push(baseSet);
        potentialMoves.push(getAllPossibleMoves(baseSet));
    }

    public List<Move> getBestMoves() {
        if (isComplete) {
            return bestMoves;
        } else {
            calculateFillingMoves();
            return bestMoves;
        }
    }

    public List<Move> getBestMovesSoFar() {
        if (isComplete) {
            return bestMoves;
        } else {
            // allow 10000 ms to compute.
            fillingMoveRunner = new Thread(new Runnable() {
                public void run() {
                    if (!isComplete) {
                        calculateFillingMoves();
                    }
                }
            });
            fillingMoveRunner.start();

            final Object tempMutex = new Object();
            synchronized (tempMutex) {
                try {
                    tempMutex.wait(10000);
                } catch (InterruptedException e) {

                }
            }
            fillingMoveRunner.interrupt();
            try {
                fillingMoveRunner.join();
            } catch (InterruptedException e) {

            }

            isComplete = true;
            return bestMoves;
        }
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void startCalculating() {
        fillingMoveRunner = new Thread(new Runnable() {
            public void run() {
                if (!isComplete) {
                    calculateFillingMoves();
                }
            }
        });
        fillingMoveRunner.start();
    }

    public void pauseCalculating() {
        if (fillingMoveRunner != null) {
            fillingMoveRunner.interrupt();
        }
    }

    private Stack<Move> getAllPossibleMoves(SegmentSet segmentSet) {
        // Assume only white owned partitions are available.
        Stack<Move> possibleMoves = new Stack<>();
        for (Segment segment : segmentSet.getWhiteOwnedSegments()) {
            for (int queenIndex : segment.getWhiteQueens()) {
                possibleMoves.addAll(segment.getPossibleMoves(queenIndex));
            }
        }
        return possibleMoves;
    }

    private void calculateFillingMoves() {
        while (true) {
            // if interrupted, exit.
            if (Thread.currentThread().isInterrupted()) {
                break;
            } else if (currentStates.empty()) {
                // no more moves to check - we're done.
                isComplete = true;
                break;
            } else if (potentialMoves.peek().empty()){
                // if there are no possible moves in this path...
                // then discard it.
                potentialMoves.pop();
                currentStates.pop();
                currentMoves.pop();
            } else {
                // get a move.
                Move potentialMove = potentialMoves.peek().pop();
                // fork the partition.
                SegmentSet newSet = currentStates.peek().forkSegmentSet(potentialMove, false);
                // get a list of moves in this new partition.
                Stack<Move> newMoves = getAllPossibleMoves(newSet);
                // Push everything.
                currentStates.push(newSet);
                potentialMoves.push(newMoves);
                currentMoves.add(potentialMove);
                // Check to see if this is the best.
                if (currentMoves.size() > bestMoves.size()) {
                    bestMoves = new LinkedList(currentMoves);
                    if (bestMoves.size() == optimalDepth) {
                        isComplete = true;
                        break;
                    }
                }
            }
        }
    }
}
