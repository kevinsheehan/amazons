package amazons.ai;

import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 */

public class InitialMoveStore {
    private SortedSet<AggregateMove> currentValues;

    private final List<MoveStore> children;

    private final CyclicBarrier barrier;

    public InitialMoveStore() {
        currentValues = Collections.synchronizedSortedSet(new TreeSet<AggregateMove>());
        children = new LinkedList<>();
        barrier = new CyclicBarrier(4);
    }

    public void addChild(MoveStore store) {
        children.add(store);
    }

    public void waitForOtherThreads() {
        try {
            barrier.await();
        } catch (InterruptedException e) {
            // reset the interrupted flag.
            Thread.currentThread().interrupt();
        } catch (BrokenBarrierException e) {

        }
    }

    public void addMove(AggregateMove move) {
        currentValues.add(move);
    }

    public AggregateMove getBestSoFar() {
        Set<AggregateMove> newValues = new HashSet<>();
        synchronized (children) {
            Iterator itr = children.listIterator();
            while(itr.hasNext()) {
                MoveStore store = (MoveStore) itr.next();
                if (store == null) continue; // I have no idea why any of these child stores would become null but...
                AggregateMove childMove = store.getRootMove();
                newValues.add(new AggregateMove(childMove.getMove(),
                        childMove.getPart(),
                        childMove.getSegmentSet(),
                        childMove.getValue()));
            }
        }
        if (currentValues.size() == 0) {
            System.err.println("Here comes a crash");
        }
        if (newValues.size() > 0) {
            int completedTotal = 0;
            AggregateMove bestMove = null;
            int bestScore = Integer.MIN_VALUE;
            Map<AggregateMove, Integer> newScoreMap = new HashMap<>();
            for (AggregateMove m : newValues) {
                newScoreMap.put(m, m.getValue());
            }
            for (AggregateMove m : currentValues) {
                if (newValues.contains(m)) {
                    completedTotal += newScoreMap.get(m) - m.getValue();
                }
            }
            int meanDelta = completedTotal / newValues.size();
            for (AggregateMove m : currentValues) {
                int tempScore;
                if (newScoreMap.containsKey(m)) {
                    tempScore = newScoreMap.get(m);
                } else {
                    tempScore = m.getValue() + meanDelta;
                }
                if (tempScore > bestScore) {
                    bestScore = tempScore;
                    bestMove = m;
                }
            }
            return bestMove;
        } else {
            return currentValues.last();
        }
    }
}
