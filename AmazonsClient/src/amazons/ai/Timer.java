package amazons.ai;

/**
 *
 */
public class Timer {

    private final double scale;

    public Timer(double scale) {
        this.scale = scale;
    }
    
    public int getMillisecondsAvailable(int secondsRemaining, int moveCount) {
        if (moveCount < 5) {
            return (int) ((scale * 2000 * secondsRemaining) / (92 - moveCount));
        }
        return (int) ((scale * 1000 * secondsRemaining) / (92 - moveCount));
    }
    
}
