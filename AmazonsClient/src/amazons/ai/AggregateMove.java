package amazons.ai;

import amazons.client.Move;
import amazons.state.Segment;
import amazons.state.SegmentSet;

/**
 */
public class AggregateMove implements Comparable<AggregateMove> {
    private final Move move;
    private final Segment segment;
    private final SegmentSet segmentSet;
    private final int value;
    public AggregateMove(Move move, Segment segment,
                         SegmentSet segmentSet, int value) {
        this.move = move;
        this.segment = segment;
        this.segmentSet = segmentSet;
        this.value = value;
    }
    public Move getMove() { return move; }
    public Segment getPart() { return segment; }
    public SegmentSet getSegmentSet() { return segmentSet; }
    public int getValue() { return value; }

    // NOTE: I am trading some proper behavior for speed here.  This will
    // only work if only moves at the same depth in the game tree are
    // compared.  This should be the case.

    public boolean equals(Object other) {
        if (other.getClass().equals(AggregateMove.class)) {
            return move.equals(((AggregateMove) other).getMove());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return move.hashCode();
    }

    public int compareTo(AggregateMove other) {
        if (value == other.getValue()) {
            // Sort based on moves, to prevent duplicate munching.
            return hashCode() - other.hashCode();
        } else {
            return value - other.getValue();
        }
    }
}
