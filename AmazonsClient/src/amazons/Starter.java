package amazons;

import amazons.client.Client;
import amazons.gui.Board;
import amazons.state.Point;
import amazons.basicstate.AmazonsState;

import java.util.ArrayList;

/**
 * @author Kevin
 */
public class Starter {

    public static void main(String[] argv) {
        String user = "DEFAULT";
        String opp = "DEFAULT";
        if (argv.length == 2) {
            user = argv[0];
            opp = argv[1];
        }
        Client client = new Client();
        
        
        /*long startTime = System.currentTimeMillis();
        // Initial Board
        // B: (0,3), (3,0), (6,0), (9,3)
        // W: (0,6), (3,9), (6,9), (9,6)
        // turn: 'W'
        char[][] initBoard = new char[10][10];
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                initBoard[i][j] = ' ';
            }
        }
        initBoard[3][0] = initBoard[0][3] = initBoard[0][6] = initBoard[3][9] = 'B';
        initBoard[6][0] = initBoard[9][3] = initBoard[9][6] = initBoard[6][9] = 'W';
        ArrayList<Point> queens = new ArrayList<>();
        queens.add(new Point(6,0));
        queens.add(new Point(9,3));
        queens.add(new Point(9,6));
        queens.add(new Point(6,9));
        client.gameState = new AmazonsState(initBoard, 'W', queens);
        ArrayList<String> moves = client.gameState.possibleMoves();
        System.out.println("Moves: " + moves.size());
        //for(String move : moves) System.out.println(move);
        long finishTime = System.currentTimeMillis();
        System.out.println("Init Time: " + (finishTime-startTime) + "ms");*/

        client.openSocket();
        client.loginAndMatch(user, opp);
        client.play();
    }
}
