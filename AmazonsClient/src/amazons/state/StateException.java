package amazons.state;

/**
 * A runtime exception for issues within the NodeSet and Segment classes
 */
public class StateException extends RuntimeException {

    /**
     * Creates a new instance of <code>StateException</code> without detail message.
     */
    public StateException() {
    }


    /**
     * Constructs an instance of <code>StateException</code> with the specified detail message.
     * 
     * @param msg the detail message
     */
    public StateException(String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>StateException</code> with the specified
     * detail message and cause.
     *
     * @param msg the detail message
     * @param cause the underlying cause
     */
    public StateException(String msg, Exception cause) {
        super(msg, cause);
    }
}
