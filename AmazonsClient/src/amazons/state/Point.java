package amazons.state;

/**
 * Point.Java
 */
public class Point {
    public int row;
    public int col;

    public Point(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public boolean equals(Point other) {
        return row == other.row && col == other.col;
    }
}
