package amazons.state;

/**
 * @author Kevin
 */
public enum SegmentState {
    DEAD,
    WHITE_OWNED,
    BLACK_OWNED,
    CONTESTED
}
