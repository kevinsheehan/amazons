package amazons.state;

import amazons.client.Move;

import java.util.*;

/**
 * Segment.Java
 *
 * A board is composed of Segments (sections of the map that are blocked off from the rest of the map)
 */
public class Segment {
    private final NodeSet referenceSet;
    private final SortedSet<Integer> enclosedSet;
    private final int generation;
    private final ModificationSet modificationSet;

    public static final int SAVE_DEPTH = 3;
    private static final boolean CACHE_ENABLED = true;

    private SegmentState state;
    private LinkedList<Integer> blackQueens;
    private LinkedList<Integer> whiteQueens;

    public Segment(NodeSet refSet, SortedSet<Integer> enclosedNodes, int gen) {
        referenceSet = refSet;
        enclosedSet = enclosedNodes;
        generation = gen;
        modificationSet = new ModificationSet();
    }

    private Segment(NodeSet refSet, SortedSet<Integer> enclosedNodes, int gen,
                      ModificationSet modSet) {
        referenceSet = refSet;
        enclosedSet = enclosedNodes;
        generation = gen;
        modificationSet = modSet;
    }

    private SortedSet<Integer> modifyEnclosed(int ro, int co, boolean fv, boolean fh, boolean sa) {
        SortedSet<Integer> newSet = new TreeSet<>();
        for (int i : enclosedSet) {
            int row = i / 10;
            int col = i % 10;
            row += ro;
            col += co;
            if (sa) { int t = row; row = col; col = t; }
            if (fh) { col = -col; }
            if (fv) { row = -row; }
            newSet.add(Node.getIndex(row,col));
        }
        return newSet;
    }

    public int enclosedCount() {
        return enclosedSet.size();
    }

    public SortedSet<Integer> getEnclosedSet() {
        return Collections.unmodifiableSortedSet(enclosedSet);
    }

    public NodeState getNodeState(int row, int col) {
        return referenceSet.getNodeState(modificationSet.modifyIndex(row, col), generation, CACHE_ENABLED);
    }

    public NodeState getNodeState(int index) {
        return referenceSet.getNodeState(modificationSet.modifyIndex(index), generation, CACHE_ENABLED);
    }

    private NodeState getFutureNodeState(int index, int gen) {
        return referenceSet.getNodeState(modificationSet.modifyIndex(index), gen, CACHE_ENABLED);
    }

    public Node getNode(int row, int col) {
        return referenceSet.getNode(modificationSet.modifyIndex(row, col), generation, CACHE_ENABLED);
    }

    public Node getNode(int index) {
        return referenceSet.getNode(modificationSet.modifyIndex(index), generation, CACHE_ENABLED);
    }

    public boolean containsNode(int row, int col) {
        return enclosedSet.contains(Node.getIndex(row, col));
    }

    public boolean containsNode(int index) {
        return enclosedSet.contains(index);
    }

    private void copyParts(Map<Integer, Integer> segmentMap, int from, int to) {
        for (int index : segmentMap.keySet()) {
            if (segmentMap.get(index).equals(from)) {
                segmentMap.put(index, to);
            }
        }
    }

    private List<Segment> getSplitSegments(int futureGen) {
        Map<Integer,Integer> segmentMap = new HashMap<>(100);
        int nextPart = 0;
        for (int i : enclosedSet) {
            // note:  this should be in order.
            NodeState iState = getFutureNodeState(i, futureGen);
            if (iState != NodeState.BLOCKED) {
                // the state is not blocked.
                // check north node.
                if (segmentMap.containsKey(i-10)) {
                    // the north node is in a partition - use it
                    segmentMap.put(i, segmentMap.get(i-10));
                    // check northwest node.
                    if ((i % 10) > 0 && segmentMap.containsKey(i-11)) {
                        // Set it to point to this partition.
                        copyParts(segmentMap, segmentMap.get(i-11), segmentMap.get(i-10));
                    }
                    // check west node.
                    if ((i % 10) > 0 && segmentMap.containsKey(i-1)) {
                        // Set it to point to this partition.
                        copyParts(segmentMap, segmentMap.get(i-1), segmentMap.get(i-10));
                    }
                    // check northeast node.
                    if ((i % 10) < 9 && segmentMap.containsKey(i-9)) {
                        // Set it to point to this partition.
                        copyParts(segmentMap, segmentMap.get(i-9), segmentMap.get(i-10));
                    }
                    // check northwest node
                } else if ((i % 10) > 0 && segmentMap.containsKey(i-11)) {
                    // reuse it
                    segmentMap.put(i, segmentMap.get(i-11));
                    // check west node.
                    if ((i % 10) > 0 && segmentMap.containsKey(i-1)) {
                        // Set it to point to this partition.
                        copyParts(segmentMap, segmentMap.get(i-1), segmentMap.get(i-11));
                    }
                    // check northeast node.
                    if ((i % 10) < 9 && segmentMap.containsKey(i-9)) {
                        // set it to point to this
                        copyParts(segmentMap, segmentMap.get(i-9), segmentMap.get(i-11));
                    }
                    // check northeast node
                } else if ((i % 10) < 9 && segmentMap.containsKey(i-9)) {
                    // reuse it
                    segmentMap.put(i, segmentMap.get(i-9));
                    // check west node.
                    if ((i % 10) > 0 && segmentMap.containsKey(i-1)) {
                        copyParts(segmentMap, segmentMap.get(i-1), segmentMap.get(i-9));
                    }
                    // check west node
                } else if ((i % 10) > 0 && segmentMap.containsKey(i-1)) {
                    segmentMap.put(i, segmentMap.get(i-1));
                } else {
                    // make a new one.
                    segmentMap.put(i, nextPart);
                    nextPart++;
                }
            }
        }
        // collect segment sets.
        Map<Integer,SortedSet<Integer>> setMap = new HashMap<>();
        for (int i : segmentMap.keySet()) {
            int p = segmentMap.get(i);
            if (!setMap.containsKey(p)) {
                setMap.put(p, new TreeSet<Integer>());
            }
            setMap.get(p).add(i);
        }
        // Create segments
        List<Segment> segments = new LinkedList<>();
        for (int p : setMap.keySet()) {
            segments.add(new Segment(referenceSet, setMap.get(p), futureGen,
                    modificationSet));
        }
        return segments;
    }

    public List<Segment> forceSplitCheck() {
        return getSplitSegments(generation);
    }

    public boolean needsSplitCheck(int index) {
        int row = index / 10;
        int col = index % 10;
        int blockedCount = 0;
        if (getNodeState(row+1,col) == NodeState.BLOCKED) {
            blockedCount++;
        }
        if (getNodeState(row-1,col) == NodeState.BLOCKED) {
            blockedCount++;
        }
        if (getNodeState(row,col+1) == NodeState.BLOCKED) {
            blockedCount++;
        }
        if (getNodeState(row,col-1) == NodeState.BLOCKED) {
            blockedCount++;
        }
        return blockedCount >= 2;
    }

    public List<Segment> forkMove(Move move, boolean isMovingPlayerBlack) {
        // This always blocks a state.
        int nextGen = referenceSet.forkMove(move, isMovingPlayerBlack, generation);
        if (needsSplitCheck(move.getShootIndex())) {
            return getSplitSegments(nextGen);
        } else {
            List<Segment> retList = new LinkedList<>();
            Segment retSegment = new Segment(referenceSet, enclosedSet, nextGen);
            retList.add(retSegment);
            return retList;
        }
    }

    public List<Segment> forkNode(int index, NodeState newState) {
        // First off, check to make sure this isn't unblocking a node.
        if (getNodeState(index) == NodeState.BLOCKED && newState != NodeState.BLOCKED) {
            throw new IllegalArgumentException("Cannot unblock a node in a segment.");

            // If the change is NOT to block something, then we don't need to search
            // for a split partition.
        } else if (newState != NodeState.BLOCKED || !needsSplitCheck(index)) {
            int nextGen = referenceSet.forkNode(index, generation, newState);
            Segment retSegment = new Segment(referenceSet, enclosedSet, nextGen);
            List<Segment> retList = new LinkedList<>();
            retList.add(retSegment);
            return retList;
        } else {
            // Make the change in the refSet.
            int nextGen = referenceSet.forkNode(index, generation, newState);

            return getSplitSegments(nextGen);
        }
    }

    private class IntPtr {
        private int _val;
        public IntPtr(int val) { _val = val; }
        public int get() { return _val; }
        public void set(int val) { _val = val; }
    }

    private class ModificationSet {
        private final int _rowOffset;
        private final int _colOffset;
        private final boolean _flipRows;
        private final boolean _flipCols;
        private final boolean _swapAxis;
        private final boolean _direct;

        public ModificationSet() {
            _rowOffset = 0;
            _colOffset = 0;
            _flipRows = false;
            _flipCols = false;
            _swapAxis = false;
            _direct = true;
        }

        public ModificationSet(int rowOffset, int colOffset, boolean flipRows,
                               boolean flipCols, boolean swapAxis) {
            _rowOffset = rowOffset;
            _colOffset = colOffset;
            _flipRows = flipRows;
            _flipCols = flipCols;
            _swapAxis = swapAxis;
            _direct = false;
        }

        public int modifyIndex(int unmodRow, int unmodCol) {
            if (_direct) {
                return Node.getIndex(unmodRow, unmodCol);
            } else {
                // FIRST, flips rows/cols.
                int row = (_flipRows ? -unmodRow : unmodRow);
                int col = (_flipCols ? -unmodCol : unmodCol);
                // SECOND, swap row/col.
                if (_swapAxis) {
                    int t = row;
                    row = col;
                    col = t;
                }
                // THIRD, apply offsets.
                return Node.getIndex(row + _rowOffset, col + _colOffset);
            }
        }

        public int modifyIndex(int unmodIndex) {
            return modifyIndex(unmodIndex / 10, unmodIndex % 10);
        }

        public int unmodifyIndex(int modRow, int modCol) {
            if (_direct) {
                return Node.getIndex(modRow, modCol);
            } else {
                // FIRST, subtract the offsets.
                int row = modRow - _rowOffset;
                int col = modCol - _colOffset;
                // SECOND, swap row/col.
                if (_swapAxis) {
                    int t = row;
                    row = col;
                    col = t;
                }
                // THIRD, flip rows/cols.
                row = (_flipRows ? -row : row);
                col = (_flipCols ? -col : col);
                return Node.getIndex(row, col);
            }
        }

        public int unmodifyIndex(int modIndex) {
            return unmodifyIndex(modIndex / 10, modIndex % 10);
        }

        public boolean isDirect() {
            return _direct;
        }
    }

    /***************************************************************************
     * ALGORITHMS                                                              *
     ***************************************************************************
     */

    public List<Integer> getReachableIndicies(int row, int col) {
        List<Integer> retList = new LinkedList<>();
        int offset = 1;
        // retList.add(Node.getIndex(row, col));
        // UP
        while (containsNode(row - offset, col) &&
                getNodeState(row - offset, col) == NodeState.EMPTY) {
            retList.add(Node.getIndex(row - offset, col));
            offset++;
        }
        offset = 1;
        // UP RIGHT
        while (containsNode(row - offset, col + offset) &&
                getNodeState(row - offset, col + offset) == NodeState.EMPTY) {
            retList.add(Node.getIndex(row - offset, col + offset));
            offset++;
        }
        offset = 1;
        // RIGHT
        while (containsNode(row, col + offset) &&
                getNodeState(row, col + offset) == NodeState.EMPTY) {
            retList.add(Node.getIndex(row, col + offset));
            offset++;
        }
        offset = 1;
        // DOWN RIGHT
        while (containsNode(row + offset, col + offset) &&
                getNodeState(row + offset, col + offset) == NodeState.EMPTY) {
            retList.add(Node.getIndex(row + offset, col + offset));
            offset++;
        }
        offset = 1;
        // DOWN
        while (containsNode(row + offset, col) &&
                getNodeState(row + offset, col) == NodeState.EMPTY) {
            retList.add(Node.getIndex(row + offset, col));
            offset++;
        }
        offset = 1;
        // DOWN LEFT
        while (containsNode(row + offset, col - offset) &&
                getNodeState(row + offset, col - offset) == NodeState.EMPTY) {
            retList.add(Node.getIndex(row + offset, col - offset));
            offset++;
        }
        offset = 1;
        // LEFT
        while (containsNode(row, col - offset) &&
                getNodeState(row, col - offset) == NodeState.EMPTY) {
            retList.add(Node.getIndex(row, col - offset));
            offset++;
        }
        offset = 1;
        // UP LEFT
        while (containsNode(row - offset, col - offset) &&
                getNodeState(row - offset, col - offset) == NodeState.EMPTY) {
            retList.add(Node.getIndex(row - offset, col - offset));
            offset++;
        }
        return retList;
    }

    public List<Integer> getReachableIndicies(int index) {
        return getReachableIndicies(index / 10, index % 10);
    }

    public List<Node> getReachableNodes(int row, int col) {
        List<Node> retList = new LinkedList<>();
        int offset = 1;
        Node t;
        // retList.add(getNode(row, col));
        // UP
        while (containsNode(row - offset, col) &&
                (t = getNode(row - offset, col)).getState() == NodeState.EMPTY) {
            retList.add(t);
            offset++;
        }
        offset = 1;
        // UP RIGHT
        while (containsNode(row - offset, col + offset) &&
                (t = getNode(row - offset, col + offset)).getState() == NodeState.EMPTY) {
            retList.add(t);
            offset++;
        }
        offset = 1;
        // RIGHT
        while (containsNode(row, col + offset) &&
                (t = getNode(row, col + offset)).getState() == NodeState.EMPTY) {
            retList.add(t);
            offset++;
        }
        offset = 1;
        // DOWN RIGHT
        while (containsNode(row + offset, col + offset) &&
                (t = getNode(row + offset, col + offset)).getState() == NodeState.EMPTY) {
            retList.add(t);
            offset++;
        }
        offset = 1;
        // DOWN
        while (containsNode(row + offset, col) &&
                (t = getNode(row + offset, col)).getState() == NodeState.EMPTY) {
            retList.add(t);
            offset++;
        }
        offset = 1;
        // DOWN LEFT
        while (containsNode(row + offset, col - offset) &&
                (t = getNode(row + offset, col - offset)).getState() == NodeState.EMPTY) {
            retList.add(t);
            offset++;
        }
        offset = 1;
        // LEFT
        while (containsNode(row, col - offset) &&
                (t = getNode(row, col - offset)).getState() == NodeState.EMPTY) {
            retList.add(t);
            offset++;
        }
        offset = 1;
        // UP LEFT
        while (containsNode(row - offset, col - offset) &&
                (t = getNode(row - offset, col - offset)).getState() == NodeState.EMPTY) {
            retList.add(t);
            offset++;
        }
        return retList;
    }

    public List<Node> getReachableNodes(int index) {
        return getReachableNodes(index / 10, index % 1);
    }

    public List<Move> getPossibleMoves(int queenIndex) {
        // Note:  This CANNOT split the partition!
        Segment emptiedPart = forkNode(queenIndex, NodeState.EMPTY).get(0);
        List<Integer> movableIndicies = getReachableIndicies(queenIndex);
        List<Move> possibleMoves = new LinkedList<>();
        for (int moveIndex : movableIndicies) {
            // Get a list of indicies from this index.
            List<Integer> shootableIndicies = emptiedPart.getReachableIndicies(moveIndex);
            // For each one, create a new move.
            for (int shootIndex : shootableIndicies) {
                possibleMoves.add(new Move(queenIndex, moveIndex, shootIndex));
            }
        }
        return possibleMoves;
    }

    public List<Integer> getNeighboringIndicies(int row, int col) {
        List<Integer> retList = new ArrayList<>(8);
        for (int pos = 0; pos <= 8; pos ++) {
            if (pos != 4) {
                int ro = (pos / 3) - 1;
                int co = (pos % 3) - 1;
                int index = Node.getIndex(row + ro, col + co);
                if (containsNode(index) && getNodeState(index) == NodeState.EMPTY) {
                    retList.add(index);
                }
            }
        }
        return retList;
    }

    public List<Integer> getNeighboringIndicies(int index) {
        return getNeighboringIndicies(index / 10, index % 10);
    }

    public List<Node> getNeighboringNodes(int row, int col) {
        List<Node> retList = new ArrayList<>(8);
        for (int pos = 0; pos <= 8; pos ++) {
            if (pos != 4) {
                int ro = (pos / 3) - 1;
                int co = (pos % 3) - 1;
                Node n;
                if (containsNode(row + ro, col + co) &&
                        (n = getNode(row + ro, col + co)).getState() == NodeState.EMPTY) {
                    retList.add(n);
                }
            }
        }
        return retList;
    }

    public List<Node> getNeighboringNodes(int index) {
        return getNeighboringNodes(index / 10, index % 10);
    }

    public SegmentState getSegmentState() {
        if (state != null) {
            return state;
        } else if (getFreeStates() == 0) {
            state = SegmentState.DEAD;
            return state;
        } else {
            boolean blackFound = false;
            boolean whiteFound = false;
            for (int i : enclosedSet) {
                NodeState iState = getNodeState(i);
                if (iState == NodeState.BLACK) {
                    blackFound = true;
                } else if (iState == NodeState.WHITE) {
                    whiteFound = true;
                }
                if (blackFound && whiteFound) break;
            }
            if (blackFound) {
                if (whiteFound) {
                    state = SegmentState.CONTESTED;
                } else {
                    state = SegmentState.BLACK_OWNED;
                }
            } else {
                if (whiteFound) {
                    state = SegmentState.WHITE_OWNED;
                } else {
                    state = SegmentState.DEAD;
                }
            }
            return state;
        }
    }

    public int getFreeStates() {
        int freeCount = 0;
        for (int i : enclosedSet) {
            NodeState iState = getNodeState(i);
            if (iState == NodeState.EMPTY) {
                freeCount++;
            }
        }
        return freeCount;
    }

    public List<Integer> getWhiteQueens() {
        if (whiteQueens != null) {
            return whiteQueens;
        } else {
            whiteQueens = new LinkedList<>();
            for (int i : enclosedSet) {
                if (getNodeState(i) == NodeState.WHITE) {
                    whiteQueens.add(i);
                }
            }
            return whiteQueens;
        }
    }

    public List<Integer> getBlackQueens() {
        if (blackQueens != null) {
            return blackQueens;
        } else {
            blackQueens = new LinkedList<>();
            for (int i : enclosedSet) {
                if (getNodeState(i) == NodeState.BLACK) {
                    blackQueens.add(i);
                }
            }
            return blackQueens;
        }
    }
}
