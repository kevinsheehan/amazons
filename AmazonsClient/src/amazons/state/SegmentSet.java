package amazons.state;

import amazons.client.Client;
import amazons.client.Move;
import amazons.gui.Board;

import java.util.*;

/**
 * This class represents a set of board segments that add up to an entire board.
 *
 * @author Kevin
 */
public class SegmentSet {
    private final List<Segment> deadSegments;
    private final List<Segment> whiteOwnedSegments;
    private final List<Segment> blackOwnedSegments;
    private final List<Segment> contestedSegments;
    public static final int SAVE_DEPTH = 3;

    public SegmentSet() {
        deadSegments = new LinkedList<>();
        whiteOwnedSegments = new LinkedList<>();
        blackOwnedSegments = new LinkedList<>();
        contestedSegments = new LinkedList<>();
    }

    public SegmentSet(Segment segment) {
        this();
        addArbitrarySegment(segment);
    }

    public SegmentSet(Collection<Segment> segments) {
        this();
        for (Segment segment : segments) {
            addArbitrarySegment(segment);
        }
    }

    public SegmentSet(List<Segment> deadParts,
                        List<Segment> whiteOwnedParts,
                        List<Segment> blackOwnedParts,
                        List<Segment> contestedParts) {
        deadSegments = deadParts;
        whiteOwnedSegments = whiteOwnedParts;
        blackOwnedSegments = blackOwnedParts;
        contestedSegments = contestedParts;
    }

    public List<Segment> getDeadSegments() {
        return Collections.unmodifiableList(deadSegments);
    }

    public List<Segment> getWhiteOwnedSegments() {
        return Collections.unmodifiableList(whiteOwnedSegments);
    }

    public List<Segment> getBlackOwnedSegments() {
        return Collections.unmodifiableList(blackOwnedSegments);
    }

    public List<Segment> getContestedSegments() {
        return Collections.unmodifiableList(contestedSegments);
    }

    public boolean areAnyContestedSegments() {
        return contestedSegments.size() > 0;
    }

    private void addArbitrarySegment(Segment segment) {
        switch (segment.getSegmentState()) {
            case DEAD:
                deadSegments.add(segment);
                break;
            case BLACK_OWNED:
                blackOwnedSegments.add(segment);
                break;
            case WHITE_OWNED:
                whiteOwnedSegments.add(segment);
                break;
            case CONTESTED:
                contestedSegments.add(segment);
                break;
            default:
                throw new StateException ("Segment has an invalid state.");
        }
    }

    private SegmentSet forkSegmentSetInternal(Segment active, List<Segment> splitSegments) {
        List<Segment> deadSet = null;
        List<Segment> whiteOwnedSet = null;
        List<Segment> blackOwnedSet = null;
        List<Segment> contestedSet = null;
        switch (active.getSegmentState()) {
            case DEAD:
                deadSet = new LinkedList(deadSegments);
                if (!deadSet.remove(active)) {
                    throw new StateException ("Segment \"active\" given to forkSegmentSet() is not registered!");
                }
                break;
            case WHITE_OWNED:
                whiteOwnedSet = new LinkedList(whiteOwnedSegments);
                if (!whiteOwnedSet.remove(active)) {
                    throw new StateException ("Segment \"active\" given to forkSegmentSet() is not registered!");
                }
                break;
            case BLACK_OWNED:
                blackOwnedSet = new LinkedList(blackOwnedSegments);
                if (!blackOwnedSet.remove(active)) {
                    throw new StateException ("Segment \"active\" given to forkSegmentSet() is not registered!");
                }
                break;
            case CONTESTED:
                contestedSet = new LinkedList(contestedSegments);
                if (!contestedSet.remove(active)) {
                    throw new StateException ("Segment \"active\" given to forkSegmentSet() is not registered!");
                }
                break;
            default:
                throw new StateException ("Segment has an invalid state.");
        }

        // Categorize these changes.
        for (Segment splitSegment : splitSegments) {
            switch (splitSegment.getSegmentState()) {
                case DEAD:
                    if (deadSet == null) {
                        deadSet = new LinkedList(deadSegments);
                    }
                    deadSet.add(splitSegment);
                    break;
                case WHITE_OWNED:
                    if (whiteOwnedSet == null) {
                        whiteOwnedSet = new LinkedList(whiteOwnedSegments);
                    }
                    whiteOwnedSet.add(splitSegment);
                    break;
                case BLACK_OWNED:
                    if (blackOwnedSet == null) {
                        blackOwnedSet = new LinkedList(blackOwnedSegments);
                    }
                    blackOwnedSet.add(splitSegment);
                    break;
                case CONTESTED:
                    if (contestedSet == null) {
                        contestedSet = new LinkedList(contestedSegments);
                    }
                    contestedSet.add(splitSegment);
                    break;
                default:
                    throw new StateException ("Segment has an invalid state.");
            }
        }

        // If any partition lists are unchanged, simply pass the reference to
        // the old one!
        if (deadSet == null) {
            deadSet = deadSegments;
        }
        if (whiteOwnedSet == null) {
            whiteOwnedSet = whiteOwnedSegments;
        }
        if (blackOwnedSet == null) {
            blackOwnedSet = blackOwnedSegments;
        }
        if (contestedSet == null) {
            contestedSet = contestedSegments;
        }

        // Make and return the new segment set!
        return new SegmentSet(deadSet, whiteOwnedSet, blackOwnedSet, contestedSet);
    }

    public Segment getContainingSegment(int index) {
        for (Segment segment : deadSegments) {
            if (segment.containsNode(index)) {
                return segment;
            }
        }
        for (Segment segment : whiteOwnedSegments) {
            if (segment.containsNode(index)) {
                return segment;
            }
        }
        for (Segment segment : blackOwnedSegments) {
            if (segment.containsNode(index)) {
                return segment;
            }
        }
        for (Segment segment : contestedSegments) {
            if (segment.containsNode(index)) {
                return segment;
            }
        }
        // Not found in any partition...
        throw new StateException ("Node with index [" + index + "] cannot be found in the partition set.");
        //return null;
    }

    public SegmentSet forkSegmentSet(Segment active, Move move, boolean isMovingPlayerBlack) {
        return forkSegmentSetInternal(active, active.forkMove(move, isMovingPlayerBlack));
    }

    public SegmentSet forkSegmentSet(Move move, boolean isMovingPlayerBlack) {
        return forkSegmentSet(getContainingSegment(Node.getIndex(move.getFromRow(),
                move.getFromCol())), move, isMovingPlayerBlack);
    }

    public List<Move> getPossibleContestedMoves(boolean isPlayerBlack) {
        List<Move> moveList = new LinkedList<>();
        for (Segment contestedPart : contestedSegments) {
            if (isPlayerBlack) {
                for (int index : contestedPart.getBlackQueens()) {
                    moveList.addAll(contestedPart.getPossibleMoves(index));
                }
            } else {
                for (int index : contestedPart.getWhiteQueens()) {
                    moveList.addAll(contestedPart.getPossibleMoves(index));
                }
            }
        }
        return moveList;
    }

    private NodeSet generateEquivalentNodeSet() {
        Map<Integer, NodeState> currentStates = new HashMap<>();
        for (Segment segment : deadSegments) {
            for (int index : segment.getEnclosedSet()) {
                currentStates.put(index, segment.getNodeState(index));
            }
        }
        for (Segment segment : whiteOwnedSegments) {
            for (int index : segment.getEnclosedSet()) {
                currentStates.put(index, segment.getNodeState(index));
            }
        }
        for (Segment segment : blackOwnedSegments) {
            for (int index : segment.getEnclosedSet()) {
                currentStates.put(index, segment.getNodeState(index));
            }
        }
        for (Segment segment : contestedSegments) {
            for (int index : segment.getEnclosedSet()) {
                currentStates.put(index, segment.getNodeState(index));
            }
        }
        NodeSet tempSet = NodeSet.BLOCKED_NODE_SET;
        int gen = 0;
        for (int index : currentStates.keySet()) {
            gen = tempSet.forkNode(index, gen, currentStates.get(index));
        }
        return tempSet.isolateGen(gen);
    }

    public SegmentSet isolate() {
        return new SegmentSet(generateEquivalentNodeSet().getRootPartition().forceSplitCheck());
    }

    public String getBoardPrintout() {
        Map<Integer, NodeState> currentStates = new HashMap<>();
        for (Segment segment : deadSegments) {
            for (int index : segment.getEnclosedSet()) {
                currentStates.put(index, segment.getNodeState(index));
            }
        }
        for (Segment segment : whiteOwnedSegments) {
            for (int index : segment.getEnclosedSet()) {
                currentStates.put(index, segment.getNodeState(index));
            }
        }
        for (Segment segment : blackOwnedSegments) {
            for (int index : segment.getEnclosedSet()) {
                currentStates.put(index, segment.getNodeState(index));
            }
        }
        for (Segment segment : contestedSegments) {
            for (int index : segment.getEnclosedSet()) {
                currentStates.put(index, segment.getNodeState(index));
            }
        }
        NodeSet printSet = NodeSet.BLOCKED_NODE_SET;
        int gen = 0;
        for (int index : currentStates.keySet()) {
            gen = printSet.forkNode(index, gen, currentStates.get(index));
        }
        Client._board.setSegmentSet(this);
        return printSet.printGen(gen);
    }

    public String getSummaryInfo() {
        StringBuilder builder = new StringBuilder();
        builder.append("State: Dead{");
        boolean isFirst = true;
        for (Segment part : deadSegments) {
            if (!isFirst) {
                builder.append(",");
            }
            builder.append(part.getFreeStates());
            isFirst = false;
        }
        builder.append("}, Opp{");
        isFirst = true;
        for (Segment part : blackOwnedSegments) {
            if (!isFirst) {
                builder.append(",");
            }
            builder.append(part.getFreeStates());
            isFirst = false;
        }
        builder.append("}, Us{");
        isFirst = true;
        for (Segment part : whiteOwnedSegments) {
            if (!isFirst) {
                builder.append(",");
            }
            builder.append(part.getFreeStates());
            isFirst = false;
        }
        builder.append("}, Contested{");
        isFirst = true;
        for (Segment part : contestedSegments) {
            if (!isFirst) {
                builder.append(",");
            }
            builder.append(part.getFreeStates());
            isFirst = false;
        }
        builder.append("}");
        return builder.toString();
    }
}
