package amazons.state;

/**
 * A representation of the state of a node.  It can either be empty, blocked, or
 * contain either the black or white queen.
 *
 */
public enum NodeState {
    EMPTY,
    BLOCKED,
    BLACK,
    WHITE
}
