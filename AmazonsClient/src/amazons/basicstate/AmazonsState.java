package amazons.basicstate;

import amazons.state.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * AmazonsState.Java
 *
 * Represents the state of the game
 *
 * THIS CLASS SHOULD BE TAKEN AS A FIRST DRAFT AND NOT NECESSARILY WHAT THE FINAL
 * REPRESENTATION OF THE GAME STATE WILL BE
 */
public class AmazonsState {
    // List<List<String>> different list for each moveable queen
    // Could see who has the most available moves
    private ArrayList<String> actions;
    private HashMap<String, AmazonsState> results;

    // I have some ideas for how to represent the board but the
    // simplest approach for now is just a 2d string array.
    private char[][] board;
    // B = black
    // W = white
    private char turn;
    // List of points of our moveable queens
    private ArrayList<Point> queens;

    public AmazonsState(char[][] board, char turn, ArrayList<Point> queens) {
        this.board = board;
        this.turn = turn;
        this.queens = queens;
    }

    public ArrayList<String> possibleMoves() {
        // Don't initialize actions until we need to know them.
        // Otherwise we go quickly down a hole.
        if (actions == null) {
            actions = new ArrayList<>();
            results = new HashMap<>();
            for (Point queen : queens) {
                // Get spots to move to
                ArrayList<Point> moves = getMovesFrom(queen);
                for (Point move : moves) {
                    // Get spots to shoot from
                    ArrayList<Point> arrows = getArrowsFrom(queen, move);
                    for (Point arrow : arrows) {
                        String s = formatMove(queen, move, arrow);
                        actions.add(s);
                        results.put(s, generateResult(queen, move, arrow));
                    }
                }
            }
        }
        return actions;
    }

    private ArrayList<Point> getMovesFrom(Point queen) {
        ArrayList<Point> moves = new ArrayList<>();
        int checkRow;
        //Check \ moves
        checkRow = queen.row+1;
        for(int col = queen.col+1; col < 10; col++) {
            if(checkRow < 0 || checkRow > 9) { break; }
            if(board[checkRow][col] == ' ') {
                moves.add(new Point(checkRow, col));
            } else {
                break;
            }
            checkRow++;
        }
        checkRow = queen.row-1;
        for(int col = queen.col-1; col >= 0; col--) {
            if(checkRow < 0 || checkRow > 9) { break; }
            if(board[checkRow][col] == ' ') {
                moves.add(new Point(checkRow, col));
            } else {
                break;
            }
            checkRow--;
        }
        //Check / moves
        checkRow = queen.row+1;
        for(int col = queen.col-1; col >= 0; col--) {
            if(checkRow < 0 || checkRow > 9) { break; }
            if(board[checkRow][col] == ' ') {
                moves.add(new Point(checkRow, col));
            } else {
                break;
            }
            checkRow++;
        }
        checkRow = queen.row-1;
        for(int col = queen.col+1; col < 10; col++) {
            if(checkRow < 0 || checkRow > 9) { break; }
            if(board[checkRow][col] == ' ') {
                moves.add(new Point(checkRow, col));
            } else {
                break;
            }
            checkRow--;
        }
        //Check - moves
        for(int col = queen.col+1; col < 10; col++) {
            if(board[queen.row][col] == ' ') {
                moves.add(new Point(queen.row, col));
            } else {
                break;
            }
        }
        for(int col = queen.col-1; col >= 0; col--) {
            if(board[queen.row][col] == ' ') {
                moves.add(new Point(queen.row, col));
            } else {
                break;
            }
        }
        //Check | moves
        for(int row = queen.row+1; row < 10; row++) {
            if(board[row][queen.col] == ' ') {
                moves.add(new Point(row, queen.col));
            } else {
                break;
            }
        }
        for(int row = queen.row-1; row >= 0; row--) {
            if(board[row][queen.col] == ' ') {
                moves.add(new Point(row, queen.col));
            } else {
                break;
            }
        }
        return moves;
    }

    /**
     * The queen param is needed so we know if we hit that spot it is actually open
     */
    private ArrayList<Point> getArrowsFrom(Point queen, Point move) {
        ArrayList<Point> arrows = new ArrayList<>();
        int checkRow;
        //Check \ moves
        checkRow = move.row+1;
        for(int col = move.col+1; col < 10; col++) {
            if(checkRow < 0 || checkRow > 9) { break; }
            if(board[checkRow][col] == ' ' || (checkRow == queen.row && col == queen.col)) {
                arrows.add(new Point(checkRow, col));
            } else {
                break;
            }
            checkRow++;
        }
        checkRow = move.row-1;
        for(int col = move.col-1; col >= 0; col--) {
            if(checkRow < 0 || checkRow > 9) { break; }
            if(board[checkRow][col] == ' ' || (checkRow == queen.row && col == queen.col)) {
                arrows.add(new Point(checkRow, col));
            } else {
                break;
            }
            checkRow--;
        }
        //Check / moves
        checkRow = move.row+1;
        for(int col = move.col-1; col >= 0; col--) {
            if(checkRow < 0 || checkRow > 9) { break; }
            if(board[checkRow][col] == ' ' || (checkRow == queen.row && col == queen.col)) {
                arrows.add(new Point(checkRow, col));
            } else {
                break;
            }
            checkRow++;
        }
        checkRow = move.row-1;
        for(int col = move.col+1; col < 10; col++) {
            if(checkRow < 0 || checkRow > 9) { break; }
            if(board[checkRow][col] == ' ' || (checkRow == queen.row && col == queen.col)) {
                arrows.add(new Point(checkRow, col));
            } else {
                break;
            }
            checkRow--;
        }
        //Check - moves
        for(int col = move.col+1; col < 10; col++) {
            if(board[move.row][col] == ' ' || (move.row == queen.row && col == queen.col)) {
                arrows.add(new Point(move.row, col));
            } else {
                break;
            }
        }
        for(int col = move.col-1; col >= 0; col--) {
            if(board[move.row][col] == ' ' || (move.row == queen.row && col == queen.col)) {
                arrows.add(new Point(move.row, col));
            } else {
                break;
            }
        }
        //Check | moves
        for(int row = move.row+1; row < 10; row++) {
            if(board[row][move.col] == ' ' || (row == queen.row && move.col == queen.col)) {
                arrows.add(new Point(row, move.col));
            } else {
                break;
            }
        }
        for(int row = move.row-1; row >= 0; row--) {
            if(board[row][move.col] == ' ' || (row == queen.row && move.col == queen.col)) {
                arrows.add(new Point(row, move.col));
            } else {
                break;
            }
        }
        return arrows;
    }

    private String formatMove(Point queen, Point move, Point arrow) {
        String s = "";
        s += "(" + queen.row + ":" + queen.col + "):";
        s += "(" + move.row + ":" + move.col + "):";
        s += "(" + arrow.row + ":" + arrow.col + ")";
        return s;
    }

    public AmazonsState generateResult(Point queen, Point move, Point arrow) {
        char[][] newBoard = new char[10][10];
        for(int i = 0; i < 10; i++)
            newBoard[i] = Arrays.copyOf(board[i], 10);
        newBoard[queen.row][queen.col] = ' ';
        newBoard[move.row][move.col] = turn;
        newBoard[arrow.row][arrow.col] = 'X';
        char newTurn = turn == 'W' ? 'B' : 'W';
        return new AmazonsState(newBoard, newTurn, moveableQueens(newBoard, newTurn));
    }

    private ArrayList<Point> moveableQueens(char[][] board, char turn) {
        // Queens of the other type that can move
        ArrayList<Point> queens = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (board[i][j] == turn) {
                    Point queen = new Point(i, j);
                    if (!getMovesFrom(queen).isEmpty()) queens.add(queen);
                }
            }
        }
        return queens;
    }

    public String whoseMove() {
        return turn == 'W' ? "White" : "Black";
    }

    public AmazonsState addMove(String move) {
        return results.get(move);
    }

    public boolean gameOver() {
        return queens.isEmpty();
    }

    public char[][] getBoard() { return board; }
}
