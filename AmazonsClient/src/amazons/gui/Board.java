package amazons.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.List;

import amazons.state.Node;
import amazons.state.NodeState;
import amazons.state.Segment;
import amazons.state.SegmentSet;
import amazons.state.SegmentState;


public class Board extends JFrame {


   //Initialize arrays to hold panels and images of the board
	private char[][] _board;
    private ChessLabel[] labels = new ChessLabel[100];
    private SegmentSet _segSet;
    private boolean _color = false;
    public Board() 
    {
    	for(int i = 0; i<=99; i++)
    	{
    		labels[i] = new ChessLabel(" ");
    	}
    	_board = new char[10][10];
    	display();
    } 
	
	public void buildBoard(List<NodeState> nodes){
		for (int i = 0; i <= 9; i++) {
            for (int j = 0; j <= 9; j++) {
                _board[i][j] = (Node.stateToChar(nodes.get(Node.getIndex(i,j))));
                switch(_board[i][j]){
	                case 'W':
	                	labels[i*10 + j].setText("\u265B");// = new ChessLabel("\u2655");
	                	labels[i*10 + j].setForeground(Color.WHITE);
	                	break;
	                case 'B':
	                	labels[i*10 + j].setText("\u265B");// = new ChessLabel("\u265B");
	                	labels[i*10 + j].setForeground(Color.BLACK);
	                	break;
	                case 'X':
	                	labels[i*10 + j].setText("X");// = new ChessLabel("\u2655");
	                	labels[i*10 + j].setForeground(Color.BLACK);
	                	break;
	                default:
	                	labels[i*10 + j].setText(" ");// = new ChessLabel(" ");
            }
            }
        }
		if (_color){
			recolor();
			colorSegments();
		}
			
	}

	public void colorSegments(){
		Segment segment;
		SegmentState state;
		for (int i = 0; i <= 99; i++)
		{
			if(!labels[i].getText().equals("X"))
			{
			segment = _segSet.getContainingSegment(i);
			state = segment.getSegmentState();
			switch(state){
				case DEAD:
					labels[i].setBackground(Color.GRAY);
					break;
				case WHITE_OWNED:
					labels[i].setBackground(Color.LIGHT_GRAY);
					break;
				case BLACK_OWNED:
					labels[i].setBackground(Color.DARK_GRAY);
					break;
				case CONTESTED:
					break;
			}
			}
				
				
		}
	}
	public void recolor(){
		int row = -1;
        for (int i = 0; i < labels.length; i++) 
        {
            if(i % 10 == 0) row ++; // increment row number
            labels[i].set(i, row);
        } // i
	}
    

    public void display()
    {
        setTitle("Amazons");

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        Container contentPane = getContentPane();
        JPanel boardPanel = new JPanel();
        JPanel controlPanel = new JPanel();
        GridLayout gridLayout = new GridLayout(10, 10);
        boardPanel.setLayout(gridLayout);
        controlPanel.setLayout(new GridLayout(1,5));

        
        JButton segBut = new JButton("Segments");
        JButton normalBut = new JButton("Normal");
        segBut.addActionListener(new ActionListener() {
        	 
            public void actionPerformed(ActionEvent e)
            {
            	colorSegments();
               _color = true;
            }
        });
        normalBut.addActionListener(new ActionListener() {
       	 
            public void actionPerformed(ActionEvent e)
            {
            	recolor();
                _color = false;
            }
        });
        
        int row = -1;
        for (int i = 0; i < labels.length; i++) 
        {
            if(i % 10 == 0) row ++; // increment row number
            labels[i].set(i, row);
           boardPanel.add(labels[i]);
        } // i
        controlPanel.add(segBut);
        controlPanel.add(normalBut);
        controlPanel.setPreferredSize(controlPanel.getPreferredSize());
        contentPane.add(boardPanel,  BorderLayout.CENTER);
        contentPane.add(controlPanel, BorderLayout.SOUTH);
        

        setSize(600, 600);
        setLocationRelativeTo(null);
        setVisible(true);
     } 

	public void setSegmentSet(SegmentSet segSet) {
		_segSet = segSet;
	}

}
