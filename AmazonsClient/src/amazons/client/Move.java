package amazons.client;

import amazons.state.Node;

/***
 * <p>Represents an anonymous Amazons move.  This class represents a simple move,
 * consisting of a starting location "from", a new location "to", and a shooting
 * location "shoot".  Each location is stored as a pair of integers for the row
 * and column.</p>
 *
 * <p>This object is an anonymous move, so the player making this move is not
 * stored in the move itself.  This information is managed by the client.</p>
 *
 * <p>This object is immutable, and thus thread-safe.</p>
 *
 * <p>This class does not verify moves.  It is up to the AI to provide valid
 * moves.  Any invalid moves will be passed to the server and will cause the
 * player to lose.</p>
 */
public class Move {
    // The old location of the queen we wish to move.
    private final int fromIndex;

    // The new location of the queen we wish to move.
    private final int toIndex;

    // The location we wish to shoot an arrow.
    private final int shootIndex;

    private int value;

    /***
     * Directly creates a new move.  Keep in mind that this object is immutable.
     * @param fromRow the old row of the moving queen
     * @param fromCol the old column of the moving queen
     * @param toRow the new row of the moving queen
     * @param toCol the new column of the moving queen
     * @param shootRow the row to shoot at
     * @param shootCol the column to shoot at
     */
    public Move(int fromRow, int fromCol, int toRow, int toCol,
                int shootRow, int shootCol)
    {
        fromIndex = Node.getIndex(fromRow, fromCol);
        toIndex = Node.getIndex(toRow, toCol);
        shootIndex = Node.getIndex(shootRow, shootCol);
        value = 0;
    }

    public Move(int fromIndex, int toIndex, int shootIndex) {
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
        this.shootIndex = shootIndex;
        value = 0;
    }

    public Move(int fromIndex, int toIndex, int shootIndex, int value) {
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
        this.shootIndex = shootIndex;
        this.value = value;
    }

    /***
     * Creates a new move by parsing a string.  The format of the string must be
     * IDENTICAL to the string sent by the server in response to a move.  This
     * constructor is only intended to be used by the client when parsing
     * opponent moves.
     *
     * @param echo the string representation of a move
     */
    public Move(String echo) {
        // Example:
        // Move:White:(6:0):(1:5):(5:9)
        // 1234567890123456789012345678
        this(Integer.parseInt(echo.substring(12,13)),
             Integer.parseInt(echo.substring(14,15)),
             Integer.parseInt(echo.substring(18,19)),
             Integer.parseInt(echo.substring(20,21)),
             Integer.parseInt(echo.substring(24,25)),
             Integer.parseInt(echo.substring(26,27)));
    }

    /***
     * Creates a string representation of this move.  This string is the exact
     * format required by the Amazons server for a move to be correctly sent.
     * This is intended to be used by the client when pushing player moves to
     * the server.
     *
     * @return a printout of the move suitable to send to the server
     */
    public String getSendingPrintout() {
        return String.format("(%d:%d):(%d:%d):(%d:%d)",
                getFromRow(), getFromCol(), getToRow(), getToCol(), getShootRow(), getShootCol());
    }

    // These are basic accessors for the fields in the ClientMove object.
    public int getFromIndex() { return fromIndex; }
    public int getFromRow() { return fromIndex / 10; }
    public int getFromCol() { return fromIndex % 10; }
    public int getToIndex() { return toIndex; }
    public int getToRow() { return toIndex / 10; }
    public int getToCol() { return toIndex % 10; }
    public int getShootIndex() { return shootIndex; }
    public int getShootRow() { return shootIndex / 10; }
    public int getShootCol() { return shootIndex % 10; }
    public int getValue() { return value; }
    
    @Override
    public boolean equals(Object other) {
        if (other.getClass().equals(Move.class)) {
            Move om = (Move) other;
            return fromIndex == om.getFromIndex() && toIndex == om.getToIndex()
                    && shootIndex == om.getShootIndex();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return fromIndex + (100 * toIndex) + (10000 * shootIndex);
    }

    public void setValue(int v) {
        value = v;
    }
}

