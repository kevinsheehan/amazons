package amazons.client;

import amazons.ai.AmazonsAI;
import amazons.ai.Heuristic;
import amazons.ai.Timer;
import amazons.basicstate.AmazonsState;
import amazons.gui.Board;
import amazons.state.Point;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Kevin
 */
public class Client {
    private final static String USER1 = "5";
    private final static String PASSWORD1 = "733167";
    private final static String USER2 = "6";
    private final static String PASSWORD2 = "431349";
    private final static String OPPONENT = "0";
    public static Board _board;
    private final String server = "icarus.engr.uconn.edu";
    private int port = 3499;
    private String color;
    private String gameId;
    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;
    // Keeps track of the current Game State
    public AmazonsState gameState;
    // What is our best next move? Update this and pull from here when time is up.
    private String bestMove;

    private AmazonsAI ai = null;

    public void openSocket(){
        //Create socket connection, adapted from Sun example code
        try{
            socket = new Socket(server, port);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (UnknownHostException e) {
            System.out.println("Unknown host: " + server);
            System.exit(1);
        } catch  (IOException e) {
            System.out.println("No I/O");
            System.exit(1);
        }
    }

    public void loginAndMatch(String user, String opp) {
        try {
            readLineAndEcho(); // start message
            if (user.equals("DEFAULT") || user.equals(USER1)) {
                readLineAndEcho(); // ID query
                writeMessageAndEcho(USER1); // user ID response
                readLineAndEcho(); // password query
                writeMessage(PASSWORD1);  // password response
            } else {
                readLineAndEcho(); // ID query
                writeMessageAndEcho(USER2); // user ID response
                readLineAndEcho(); // password query
                writeMessage(PASSWORD2);  // password response
            }
            readLineAndEcho(); // opponent query
            if (opp.equals("DEFAULT")) {
                writeMessageAndEcho(OPPONENT);  // opponent response
            } else {
                writeMessageAndEcho(opp);
            }
            gameId = readLine().substring(5, 9); // game
            color = readLine().substring(6, 11);  // color
            System.out.println("I am playing as " + color + " in game number " + gameId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void play() {
    	_board = new Board();
    	//_board.display();
        Heuristic eval = new Heuristic();
        Timer timer = new Timer(2);
        ai = new AmazonsAI(color.equals("Black"), eval, timer, 2, 5);
        String message = "";
        try {
            while ((message = readLine()) != null && !socket.isClosed()) {
                if (message.startsWith("Move:")) {
                    if (message.startsWith("Move:"+color)) continue;
                    Move move = new Move(message);
                    ai.opponentMove(move);
                    /*String move = message.substring(11);
                    System.out.println(move);
                    extractMove(move);*/
                } else if (message.startsWith("?Move")) {
                    int secondsLeft = Integer.parseInt(message.substring(6,message.length() - 2));
                    Move move = ai.getPlayerMoveAbstract(secondsLeft);
                    writeMessageAndEcho(move.getSendingPrintout());
                    /*printBoard();
                    Random rand = new Random();
                    String move = gameState.possibleMoves().get(rand.nextInt(gameState.possibleMoves().size()));
                    gameState = gameState.addMove(move);*/
                } else if (message.startsWith("Result")) {
                    if (message.substring(7).equals(color)) {
                        System.out.println("We Won");
                    } else {
                        System.out.println("We Lost");
                    }
                    disconnect();
                }
                //System.gc();
            }
        } catch (IOException e) {
            disconnect();
            System.err.println("IOException: " + e.getMessage());
        }
    }

    public String readLineAndEcho() throws IOException {
        String readMessage = in.readLine();
        System.out.println("In: " + readMessage);
        return readMessage;
    }

    public String readLine() throws IOException {
        return in.readLine();
    }

    public void writeMessage(String message) throws IOException {
        out.print(message + "\r\n");
        out.flush();
    }

    public void writeMessageAndEcho(String message) throws IOException {
        out.print(message + "\r\n");
        out.flush();
        System.out.println("Out: " + message);
    }

    private void extractMove(String move) {
        // RegEx match:
        // .*?(\d),(\d).*?\((\d),(\d).*?\((\d),(\d).*
        Pattern regex = Pattern.compile(".*?(\\d):(\\d).*?(\\d):(\\d).*?(\\d):(\\d).*");
        Matcher matcher = regex.matcher(move);
        matcher.find();
        Point queen = new Point(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
        Point pos = new Point(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)));
        Point arrow = new Point(Integer.parseInt(matcher.group(5)), Integer.parseInt(matcher.group(6)));
        gameState = gameState.generateResult(queen, pos, arrow);
    }

    private void printBoard() {
        char[][] board = gameState.getBoard();
        for(char[] row : board) {
            System.out.print("|");
            for(char column : row) {
                System.out.print("_"+ column +"_|");
            }
            System.out.println();
        }
    }

    private void disconnect() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) { }
    }
}
