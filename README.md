# Amazons Client #
This game client was developed for an AI class at UConn where bots were pitted against one another in a round robin tournament of [Game of the Amazons](http://en.wikipedia.org/wiki/Game_of_the_Amazons). The rules and objective of the game are both rather simple but actually winning it consistently is a challenge.

### Results ###
Sadly, this bot didn't do as well as hoped. A flaw in the synchronization of the threads led to a couple of losses due to unhandled exceptions. This landed the bot at about middle of the pack in the tournament.